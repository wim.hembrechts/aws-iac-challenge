<?php

/*
*  Instance Configuration
*  ----------------------
*  Edit this file and not config.php for imageboard configuration.
*
*  You can copy values from config.php (defaults) and paste them here.
*/
    $$config['db']['server'] = '${RDS_URL}';
    $$config['db']['database'] = '${DB_NAME}';
    $$config['db']['prefix'] = '';
    $$config['db']['user'] = '${DB_USERNAME}';
    $$config['db']['password'] = '${DB_PASSWORD}';


    $$config['cookies']['mod'] = 'mod';
    $$config['cookies']['salt'] = 'OSSL.7qDyVmEnU8DFYFRSuZimHtxHFUZl2m3yhA9XnNG3HBlkgpIjN3iWslE2X27dlJ+oM1EPm5NZCYLzsYVFZoM87QmBi7WH/O7sVIuAnEBs5rUxyfqaQYBkiqsnF/3mU5T36mbdoyE7oB/iNDbfIf2AUXto2ULzcrpadWQZNun3xGU=';

    $$config['flood_time'] = 10;
    $$config['flood_time_ip'] = 120;
    $$config['flood_time_same'] = 30;
    $$config['max_body'] = 1800;
    $$config['reply_limit'] = 250;
    $$config['max_links'] = 20;
    $$config['max_filesize'] = 10485760;
    $$config['thumb_width'] = 255;
    $$config['thumb_height'] = 255;
    $$config['max_width'] = 10000;
    $$config['max_height'] = 10000;
    $$config['threads_per_page'] = 10;
    $$config['max_pages'] = 10;
    $$config['threads_preview'] = 5;
    $$config['root'] = '/';
    $$config['secure_trip_salt'] = 'OSSL.+HU3RHnfnRcTRwoUClULa55Ehksb7IZdoCRGfgde4uB8Recf50BErMnBycyI9/HDQ96CIGWPXwcyG5JHwl888yXcncBjQmxLtHEexdDoAnpcvKaJBUMbbOdgci2xMN2RGyo6WNd/9q0bUOpzwJs6mrrAqbOUn/gKYq3BBlmX7ng=';

    $$config['thumb_method'] = 'gm+gifsicle';
    $$config['gnu_md5'] = '1';
    $$config['board_path'] = 'boards/%s/';