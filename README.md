![header](data/header.jpg)
# AWS IaC Challenge
## Intro

In our CloudEngineering classes we have learned to apply the DevOps principles into our workflow. For this we learned to work with GitLab, Git, Terraform, API's and much more.

To prove our proficiency in these matters, we were tasked to deploy a web application with database on AWS. We have to use as much IaC(Infrastructure as code) as possible, this means we will have to use Terraform to provision our resources and use the GitLab CI/CD pipeline to automate the testing and deploying of our code.

## Feature overview

To make this project easier to understand, we will explain our different functionalities and how to use them in this readme. We have divided this readme as such:

*   [ ] **Overview of our infrastructure**
*   [ ] **Getting started**
*   [ ] **WebApp** deployed with **Docker**
*   [ ] **Terraform**
*   [ ] **GitLab CI**
*   [ ] **Domain name from vimexx (personal-tristan)**
*   [ ] **Discord webhook**

## Contents

*   [Overview](#overview)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
*   [Docker and WebApp](#docker-and-webapp)
*   [Terraform](#terraform)
    * [EB](#elastic-beanstalk)
    * [RDS](#rds)
    * [EFS](#efs)
    * [VPC](#vpc)
    * [Security Groups](#security-groups)
*   [GitLab CI](#gitlab-ci)
*   [Domain name from vimexx](#domain-name-from-vimexx)
*   [Discord](#discord-webhook)



## Overview
![schema](data/infrastructuur.jpg)

As you can see in the picture above, we have used the following components in our network:

* **Elastic Beanstalk**(Consisting of EC2, S3, loadbalancer, autoscaling group)
* **RDS database**(Relational database service)
* **EFS**(Elastic Filesystem) 
* **VPC**(Consisting of subnets in all the availability zones, an internet gateway, and a routing table)
* We've also applied security groups to our EB and our EFS

As application to deploy we picked Vichan. Vichan is an imageboard where people can post pictures and make threads to converse with eachother.
This can happen anonymously if desired. The application mostly uses PHP, and a little javascript aswell, however you don't need to have Javascript enabled to browse the imageboard.

We've used Elastic Beanstalk to host this app, the app will be delivered by a docker container, which we've made with a Dockerfile.

As database we've used RDS and coupled this to our Elastic Beanstalk environment. To make the database persistent we used Snapshots.

Because the pictures posted on the imageboard are stored in the container locally, we had to find a way to make this persistent aswell. For this we decided to automount EFS to our EC2 container(with the help of .ebexstensions),
and then used the VOLUME option in docker to link this mountpoint to the directory inside the docker container where our images reside.

In our VPC we made subnets for all the availability zones, and Security rules were also applied. One rule to allow SSH and HTTP traffic to the E2 instance and one rule to allow EFS traffic from and to the EFS filesystem.

In our Gitlab CI, we've integrated Discord notifications to notify us of changes made in the project.

To test our application, DAST(Dynamic Application Security Testing) is also used. For this StackHawk HawkScan was used.

## Getting Started

How do we use this in our own environment?

### Requirements

* You will need a gitlab account
* You need to have access to an AWS account

### Install

Use git to clone this repository.

``` bash
git clone https://gitlab.com/it-factory-thomas-more/cloud-engineering/22-23/iac-team-1/aws-iac-challenge-team-1.git
```
Before you can run the pipeline, you first will need to fill in the proper variables into your gitlab Vault:

*	AWS_ACCESS_KEY_ID
*	AWS_ACCOUNT_ID
*	AWS_DEFAULT_REGION
*	AWS_SECRET_ACCESS_KEY
*	AWS_SESSION_TOKEN
*	DB_NAME
*	DB_PASSWORD
*	DB_USERNAME
*	EB_APP_NAME
*	EB_ENV_NAME
*	GITLAB_API_TOKEN
*	GITLAB_DEPLOY_TOKEN
* GITLAB_PROJECT_ID
* GITLAB_USERNAME
* SECRETS_PHP_CONFIG


## Docker and WebApp

To get our application working in Elastic Beanstalk, we chose to package this into a docker container. 
We first created a dockerfile:

``` dockerfile
FROM r0853681/ubuntu-apache-php:latest

ENV TZ=Europe/Brussels
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN mkdir /var/www/html/vichan
WORKDIR /var/www/html/vichan
COPY ./vichan /var/www/html/vichan
COPY ./init.sh /var/www/html/init.sh

RUN sed -i 's/Options Indexes FollowSymLinks/#Options Indexes FollowSymLinks/' /etc/apache2/apache2.conf
RUN sed -i 's+DocumentRoot /var/www/html+DocumentRoot /var/www/html/vichan+' /etc/apache2/sites-available/000-default.conf
RUN sed -i 's/: 0;/: 4;/' /var/www/html/vichan/install.php

RUN cd /var/www/html/vichan
RUN mkdir cache
RUN mkdir boards
RUN service apache2 start
RUN composer install

RUN chmod -R 755 /var/www/html/vichan
RUN chown www-data:www-data -R /var/www/html/vichan
RUN chmod +x /var/www/html/init.sh

CMD ["/bin/sh", "-c", "/var/www/html/init.sh"]
```

In the dockerfile we created the necessary directories, permissions, etc,...
At the end we made sure to have the docker container run a script after creation.
In this script we run the install script, which we've modified so the script can be ran headless(Originally the installation had to be done through the webinterface of Vichan).

To then deploy it to AWS, we used the dockerrun.aws.json file and the auth.json file:

```json
{
    "AWSEBDockerrunVersion": "1",
    "Image": {
        "Name": "${CI_REGISTRY_IMAGE}:${CI_PIPELINE_IID}"
    },
    "Authentication": {
        "Bucket": "elasticbeanstalk-${AWS_DEFAULT_REGION}-${AWS_ACCOUNT_ID_JORDY}",
        "Key": "/auth.json"
    },
    "Ports": [
        {
            "ContainerPort": 80
        }
    ],
    "Volumes": [
    {
      "HostDirectory": "/boards",
      "ContainerDirectory": "/var/www/html/vichan/boards"
    }
  ]
}
```

In the dockerrun file we provide our image, which we've pushed to the gitlab registry, which ports need to be opened, the volume we mounted, for persistant storage, and how to authenticate to the S3 bucket, to which out app will first be sent, before it gets deployed on the EC2 instance.

In the auth.json file we give the deploy token AWS needs, to access the image:

```json
{
    "auths": {
        "$CI_REGISTRY": {
            "auth": "$GITLAB_DEPLOY_TOKEN"
        }
    }
}
```

To browse to your application you will first have to go to the website through the public dns name of our Elastic Beanstalk:
```bash
http://dnsname.com/mod.php
```
And login to our app with the default credentials of:
* user: admin
* password: password

Now you can edit and create new boards.

## Terraform

Terraform is an open source "Infrastructure as Code" tool, created by HashiCorp. It is a declarative coding tool.
Meaning that you don't write the actual code yourself, declare what the code needes to do.

Terraform has an AWS Provider so you can create resources within AWS using this provider.
We will be using this provider for all the Terraform files.

### Variables

Like with every program you need to declare variables. Terraform is no different, you also need to declare variables. The main reason is security to make sure that you don't have any credentials or important data in you public files. We use these variables in our project:
```terraform
# Provider variables
variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "aws_session_token" {}
variable "aws_account_id" {}
variable "aws_default_region" {}

# Database variables
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}

# Elastic Beanstalk variables
variable "eb_app_name" {}
variable "eb_env_name" {}

# Pipeline Version
variable "ci_pipeline_iid" {}
```
All these variables will be filled in by our GitLab pipeline by variables in the GitLab Vault.

### Elastic Beanstalk

With Elastic Beanstalk you can quickly deploy and manage applications in AWS without having to learn about the infrastructure that runs those applications.

Elastic Beanstalk supports applications developed in Go, Java, .NET, Node.js, PHP, Python, and Ruby. When you deploy your application, Elastic Beanstalk builds the selected supported platform version and provisions one or more AWS resources, such as Amazon EC2 instances, to run your application.

To use Elastic Beanstalk, you create an application, upload an application version in the form of an application source bundle (for example, a Java .war file) to Elastic Beanstalk, and then provide some information about the application. Elastic Beanstalk automatically launches an environment and creates and configures the AWS resources needed to run your code. After your environment is launched, you can then manage your environment and deploy new application versions. The following diagram illustrates the workflow of Elastic Beanstalk.

#### Elastic Beanstalk Application
In de following code we declare the Elasti Beanstalk Application
```terraform
resource "aws_elastic_beanstalk_application" "vichan-app" {
    name                        = "${var.eb_app_name}"
    description                 = "Vichan application"

    appversion_lifecycle {
        service_role            = "arn:aws:iam::${var.aws_account_id}:role/LabRole"
    }
}
```
To be able to deploy an application via Elastic Beanstalk we need a special kind of permissions. That is why we use the service role. These permissions are needed to create the required autoscaling groups that Elastic Beanstalk uses to scale the application.

#### Elastic Beanstalk Environment

Now that we declared the application that we would like to run we need to declare the environment on which the application is supposed to run.
```terraform
resource "aws_elastic_beanstalk_environment" "vichan-env" {
    name                    = "${var.eb_env_name}"
    application             = aws_elastic_beanstalk_application.vichan-app.name
    solution_stack_name     = "64bit Amazon Linux 2 v3.5.2 running Docker"

    setting {
        namespace   = "aws:elasticbeanstalk:environment"
        name        = "ServiceRole"
        value       = "arn:aws:iam::${var.aws_account_id}:role/LabRole"
    }
    ...
    # not all configuration is shown
```
Within the Elastic Beanstalk environment we need to give a lot of extra settings to make sure our environment is declared correctly for our project.

For instance we create a custom vpc. For this we also needs some extra configuration.
```terraform
    setting {
        namespace   = "aws:ec2:vpc"
        name        = "VPCId"
        value       = aws_vpc.vpc.id
    }
    setting {
        namespace   = "aws:ec2:vpc"
        name        = "ELBScheme"
        value       = "public"
    }
    ...
    # not all configuration is shown
```

#### Database Instance
We also create an automatically connected DB Instance with our Elastic Beanstalk. This database is user by our Vichan application. The database that we use is a MySQL database with 20GB storage. When the Elastic Beanstalk application is being deleted the database makes a snapshot and stores it in AWS. When the a new application is being created this snapshot is used to restore the database of the Vichan application.
```terraform
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBAllocatedStorage"
        value       = "20"
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBEngine"
        value       = "mysql"
    }
    ...
    # not all configuration is shown
```

### Elastic Filesystem

The elastic filesystem is a serverless filesystem which you can use across your entire cloud environment.
It automatically grows and shrinks as you add and remove files with no need for management or provisioning.

Because this is such a flexible filesystem, we chose to use this as our persistent data store for our application. Unfortunately we didn't manage to mount the filesystem automatically, because we ran across some errors.

The process to do this goes like this:
* Create an .ebextensions folder with the script to mount the filesystem to our EC2 container.
* The .ebextensions folder will be run everytime a new EC2 container spins up, thus making the filesystem always available.
* Due to constraints of Elastic Beanstalk, we can't deploy the .ebextensions directory together with our json files(dockerrun and auth), so we had to zip the files and then send them to S3.

This gave us an error, saying it couldn't find the auth.json file, the filesystem got mounted, and the dockerrun got executed, but our container failed eventually.

So now we have to manually mount the filesystem before the creation of the container on our EC2, which we didn't get to implement in our GitLab CI in time due to time constraints.
All files are in place to implement this however, and we chose to still leave the creation of the EFS in our pipeline.

In the terraform file for EFS we have to create the filesystem itself, an access point, and the mount target.
Optionally, we also entered in a policy, where we give read/write access to everyone with the right Role.

The terraform file for the EFS:

```terraform
resource "aws_efs_file_system" "myfilesystem" {
  creation_token = "boards"
  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }
  encrypted = "true"
  tags = {
    Name = "BoardsFilesystem"
  }
}

resource "aws_efs_access_point" "efs_ap" {
  file_system_id = aws_efs_file_system.myfilesystem.id
}

resource "aws_efs_file_system_policy" "policy" {
  file_system_id = aws_efs_file_system.myfilesystem.id

  policy = <<POLICY
    ...
POLICY
}

resource "aws_efs_mount_target" "boards_mount" {
    ...
}

```


### VPC

A VPC(Virtual Private Cloud) is basically a seperate network inside your cloud environment. This helps us to segment our network, we also added the subnets from all availabilty zones.
This to make our EFS and EB redundant across the availability zones.

First we created a vpc with 10.0.0.0/16
We then declare the subnets for all the availability zones.
To give our VPC access to the internet, we also had to create an internet gateway, with its associated routing tables.

```terraform
data "aws_availability_zones" "available" {}

resource "aws_vpc" "vpc" {
    ...
 }
resource "aws_subnet" "subnet" {
    ...
  }

resource "aws_internet_gateway" "internet_gateway" {
    ...
}

resource "aws_route_table" "public" {
    ...
  }

  tags = {
    Name = "Public Route Table"
  }
  depends_on = [aws_internet_gateway.internet_gateway]
}

resource "aws_route_table_association" "vpc_public_assoc" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = aws_subnet.subnet.*.id[count.index]
  route_table_id = aws_route_table.public.id
}
```

### Security Groups
For our security groups we made two groups, one to give the EC2 access to ssh and http, aswell as one to give our EC2 access to the EFS.
Ingress are the outbound rules, we set 22 and 80 to be allowed, egress if for the outbound rules, we decided to let all outbound traffic through.

```terraform
 resource "aws_security_group" "ec2" {
  name        = "allow_efs"
  description = "Allow efs outbound traffic"
  vpc_id      = aws_vpc.vpc.id
  ingress {
     cidr_blocks = ["0.0.0.0/0"]
     from_port = 22
     to_port = 22
     protocol = "tcp"
   }
  ingress {
     cidr_blocks = ["0.0.0.0/0"]
     from_port = 80
     to_port = 80
     protocol = "tcp"
   }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_efs"
  }
}

resource "aws_security_group" "efs" {
   name = "efs-sg"
   description= "Allows inbound efs traffic from ec2"
   vpc_id = aws_vpc.vpc.id

   ingress {
     security_groups = [aws_security_group.ec2.id]
     from_port = 2049
     to_port = 2049 
     protocol = "tcp"
   }     
        
   egress {
     security_groups = [aws_security_group.ec2.id]
     from_port = 0
     to_port = 0
     protocol = "-1"
   }
 }
 ```


## GitLab CI/CD
GitLab CI/CD is a tool for software development using the continous methodologies:
    - Continuous Integration (CI)
    - Continuous Delivery (CD)
    - Continuous Deployment (CD)

GitLab CI/CD makes use of Runners that run the declared jobs that you configured in your pipeline. This pipeline configuration is written in a ".gitlab-ci.yml" file.


### Our .gitlab-ci.yml
The first thing that we need to do in this file is configure the stages we want to use.
```yaml
stages:
  - before-tf
  - tf-validate
  - tf-plan
  - tf-apply
  - vichan-secrets
  - vichan-package
  - vichan-deploy
  - tf-destroy
```

#### Default
We created a default image and a default before_script. These are used by every Terraform stage. The image we declared here is an image that our Terraform stages can use. The before script is a script that runs in every Terraform stages before the main script.

#### Stage: before-tf
The before tf stage is a stage that we need to run before the Terraform stages. This stages reads the if a database snapshot exists. If a snapshot exists it forwards the snapshot identifier to the next stages.

#### Stage: tf-validate
Terraform has a validate function. This function validates all the Terraform file in the repository.

#### Stage: tf-plan
Terraform plan checks the Terraform Statefile that is saved in GitLab. The plan stage uses this file to check which resources already exist in AWS and which still need to be created. It saves the output in a planfile. This plan file is than forwarded to the next stage.

#### Stage: tf-apply
Terraform apply uses the previously made planfile to create or modify the resources declared in this file. After this apply Terraform updates the statefile and stores it again in GitLab to be used in the next pipeline.

#### Stage: vichan-secrets
In this stages find the databse url of the newly created database instance. Our application needs this url to locate the database. This url is replaced in the secrets.php file of our application.

#### Stage: vichan-package
This stage is used to build our Docker image. After this Docker image is build and created it is stored in the container registry of GitLab.

#### Stage: vichan-deploy
This stage is used to deploy the actual application to our Elastic Beanstalk environment that we created during the tf-apply stage. This is the "last" stage of our pipeline. Only the destroy stage is after this one.

#### Stage: tf-destroy
The name tf-destroy stage says it all. The only thing this stage does is delete the whole Vichan application and all our resources created on AWS.



## Domain name from vimexx
This code is ready in personal-tristan branch.

We wanted to use our own domain name for this assignment. To achieve this we used a zone in route 53. This way we could connect to vimexx where we already had a domain.


We needed a fixed name server from aws hence we did this through the GUI. First, we create a zone in AWS this is done by going to route 53. 

![hosted_zone](data/AWS_hosted_zone.jpg)

Then click in the hosted zone. Here you will see the record with the name servers that belong to this hosted zone.

![Name_server](data/AWS_name_servers.jpg)

After copying, you can go to vimexx. Go to the domain you want to link to AWS and click here you will get the option to set your name server. Paste the name servers from aws here.

![vimexx](data/vimex_name_server.jpg)

Of course, we didn't want to do everything through the GUI so we do use terraform to create the records.

```terraform

data "aws_route53_zone" "technisme_zone" {
  name          = var.domain_name
  private_zone  = false

}

data "aws_eip" "by_tags" {
  tags = {
    Name = "vichan-app"
  }
}

resource "aws_route53_record" "www" {
  zone_id = var.zone_id
  name    = var.domain_name
  type    = "A"
  ttl     = 300
  records = [aws_eip.by_tags.public_ip]
  allow_overwrite = true
}


resource "aws_route53_record" "www2" {
  zone_id = var.zone_id
  name    = "www.technisme.com"
  type    = "CNAME"
  ttl     = 300
  records = [var.domain_name]
  allow_overwrite = true
}
```


## Discord webhook

We wanted to get messages from our piplines and the states in our discord channel. We use a webhook in discord and connected it using gitlab integrations.

Create a webhook in discord and copy the link at the bottom.
![discord_webhook](data/webhook_discord.jpg)

Then paste the link into gitlab under integrations choose the discord option.
![discord_webhook](data/integraties_gitlab.jpg)



