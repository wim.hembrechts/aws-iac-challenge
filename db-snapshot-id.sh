#!/bin/sh

aws rds describe-db-snapshots --snapshot-type manual --output text > dbsnapshots.txt

if [ -s dbsnapshots.txt ]; then
    i=0
    while read line; do
        i=$((i+1))
        snapshotid=$(echo $line | cut -c 190-252)
        creation_timestamp=$(echo $line | cut -c 430-449)
        date=$(echo $creation_timestamp | cut -c 1-10)
        time=$(echo $creation_timestamp | cut -c 12-19)
        epoch=$(date -d "${date} ${time}" +"%s")
        
        if [ $i -eq 1 ]; then
            highest=$epoch
            db_snapshot_id=$snapshotid
        elif [ $epoch -gt $highest ]; then
            db_snapshot_id=$snapshotid
        fi
    done < dbsnapshots.txt

    export DB_SNAPSHOT_ID=$db_snapshot_id
    cp infrastructure/elastic-beanstalk.tf elastic-beanstalk.tf
    envsubst < elastic-beanstalk.tf > infrastructure/elastic-beanstalk.tf
else
    sed -i '110s/ /# /' infrastructure/elastic-beanstalk.tf
    sed -i '111s/ /# /' infrastructure/elastic-beanstalk.tf
    sed -i '112s/ /# /' infrastructure/elastic-beanstalk.tf
    sed -i '113s/ /# /' infrastructure/elastic-beanstalk.tf
    sed -i '114s/ /# /' infrastructure/elastic-beanstalk.tf
fi