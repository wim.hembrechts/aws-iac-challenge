# Provider variables
variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "aws_session_token" {}
variable "aws_account_id" {}
variable "aws_default_region" {}

# Database variables
variable "db_username" {}
variable "db_password" {}

# Elastic Beanstalk variables
variable "eb_app_name" {}
variable "eb_env_name" {}

# Version
variable "ci_pipeline_iid" {}