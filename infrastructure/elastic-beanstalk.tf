resource "aws_elastic_beanstalk_application" "vichan-app" {
    name                        = "${var.eb_app_name}"
    description                 = "Vichan application"

    appversion_lifecycle {
        service_role            = "arn:aws:iam::${var.aws_account_id}:role/LabRole"
    }
}

resource "aws_elastic_beanstalk_environment" "vichan-env" {
    name                    = "${var.eb_env_name}"
    application             = aws_elastic_beanstalk_application.vichan-app.name
    solution_stack_name     = "64bit Amazon Linux 2 v3.5.2 running Docker"

    setting {
        namespace   = "aws:elasticbeanstalk:environment"
        name        = "ServiceRole"
        value       = "arn:aws:iam::${var.aws_account_id}:role/LabRole"
    }
    setting {
        namespace   = "aws:elasticbeanstalk:environment"
        name        = "LoadBalancerType"
        value       = "application"
    }
    setting {
        namespace   = "aws:autoscaling:launchconfiguration"
        name        = "SecurityGroups"
        value       = aws_security_group.ec2.id
    }
    setting {
        namespace   = "aws:autoscaling:launchconfiguration"
        name        = "IamInstanceProfile"
        value       = "arn:aws:iam::${var.aws_account_id}:instance-profile/LabInstanceProfile"
    }
    setting {
        namespace   = "aws:autoscaling:launchconfiguration"
        name        = "EC2KeyName"
        value       = "vockey"
    }
    setting {
        namespace   = "aws:ec2:vpc"
        name        = "VPCId"
        value       = aws_vpc.vpc.id
    }
    setting {
        namespace   = "aws:ec2:vpc"
        name        = "ELBScheme"
        value       = "public"
    }
    setting {
        namespace   = "aws:ec2:vpc"
        name        = "ELBSubnets"
        value       = join(",",[aws_subnet.subnet[0].id,aws_subnet.subnet[1].id,aws_subnet.subnet[2].id,aws_subnet.subnet[3].id,aws_subnet.subnet[4].id,aws_subnet.subnet[5].id])
    }
        setting {
        namespace   = "aws:ec2:vpc"
        name        = "Subnets"
        value       = join(",",[aws_subnet.subnet[0].id,aws_subnet.subnet[1].id,aws_subnet.subnet[2].id,aws_subnet.subnet[3].id,aws_subnet.subnet[4].id,aws_subnet.subnet[5].id])
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBAllocatedStorage"
        value       = "20"
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBEngine"
        value       = "mysql"
    }
    setting {
        namespace   = "aws:autoscaling:asg"
        name        = "MinSize"
        value       = 1
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBEngineVersion"
        value       = "8.0.31"
    }
    setting {
        namespace   = "aws:autoscaling:asg"
        name        = "MaxSize"
        value       = 2
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBInstanceClass"
        value       = "db.t2.micro"
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBUser"
        value       = "${var.db_username}"
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBPassword"
        value       = "${var.db_password}"
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "HasCoupledDatabase"
        value       = true
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBDeletionPolicy"
        value       = "Snapshot"
    }
    setting {
        namespace   = "aws:rds:dbinstance"
        name        = "DBSnapshotIdentifier"
        value       = "${DB_SNAPSHOT_ID}"
    }
    setting {
        namespace   = "aws:ec2:vpc"
        name        = "AssociatePublicIpAddress"
        value       = true
    }
}